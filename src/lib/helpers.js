/**
 * Helper functions, for work with async storage and etc.
 */

import AsyncStorage from '@react-native-community/async-storage';
import { Bookmarks } from './../data/Bookmarks';

// settings parameters changes save
export const saveSettingToAsyncStorage = (name, value) => {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.setItem(String(name), JSON.stringify(value));
      resolve();
    } catch (error) {
      console.warn(error);
      reject(error);
    }
  });
};

// read settings paramters from a.storage
export const readSettingsAndBookmarkFromAsyncStorage = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const [arabCheck, azCheck, fontSize, data] = await Promise.all([
        AsyncStorage.getItem('arabCheck'),
        AsyncStorage.getItem('azCheck'),
        AsyncStorage.getItem('fontSize'),
        AsyncStorage.getItem('bookmark'),
      ]);
      resolve({
        arabCheck: JSON.parse(arabCheck) == null ? true : JSON.parse(arabCheck),
        azCheck: JSON.parse(azCheck) == null ? true : JSON.parse(azCheck),
        fontSize: Number(fontSize) || 22,
        bookmarks: data == null ? Bookmarks : JSON.parse(data),
      });
    } catch (error) {
      console.warn(error);
      reject(error);
    }
  });
};

// bookmark save to A.storage
export const saveBookmark = bookmarks => {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.setItem('bookmark', JSON.stringify(bookmarks));
      resolve();
    } catch (error) {
      reject(error);
    }
  });
};
