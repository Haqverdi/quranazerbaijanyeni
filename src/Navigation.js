import { createAppContainer, createStackNavigator } from 'react-navigation';

// screens
import Home from './screens/Home';
import Read from './screens/Read';
import Settings from './screens/Settings';

// main app
export default createAppContainer(
  createStackNavigator(
    {
      Home: {
        screen: Home,
      },
      Read: {
        screen: Read,
      },
      Settings: {
        screen: Settings,
      },
    },
    {
      initialRouteName: 'Home',
    }
  )
);
