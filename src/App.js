import React, { Component, Fragment } from 'react';
import { Platform, StatusBar } from 'react-native';
import Navigation from './Navigation';

export default class App extends Component {
  render() {
    return (
      <Fragment>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <Navigation />
      </Fragment>
    );
  }
}
