/**
 * Home screen Header component
 */
import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Platform,
  Keyboard,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export class HomeHeader extends PureComponent {
  render() {
    return (
      <TouchableOpacity
        style={style.mainView}
        activeOpacity={1}
        onPress={() => Keyboard.dismiss()}
      >
        <Text style={style.appName}>QURANİ KƏRİM</Text>
        <TouchableOpacity
          style={style.settingsBtn}
          onPress={() => this.props.goToSettingsScreen()}
        >
          <Icon name="settings" size={22} style={style.settingIcon} />
        </TouchableOpacity>
        <View style={style.searchBlock}>
          <Icon name="magnify" size={20} style={style.searchIcon} />
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Axtar.."
            placeholderTextColor="white"
            style={style.searchInput}
            onChangeText={this.props.handleSearch}
          />
        </View>
      </TouchableOpacity>
    );
  }
}

// styles
const style = StyleSheet.create({
  mainView: {
    backgroundColor: '#00C88C',
    borderBottomWidth: 1,
    borderBottomColor: '#dddddd',
    height: 100,
  },
  appName: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  searchBlock: {
    flexDirection: 'row',
    padding: 0,
    backgroundColor: 'transparent',
    marginHorizontal: 15,
    borderRadius: 5,
    borderColor: 'white',
    height: 40,
    borderWidth: 1,
    marginTop: Platform.OS == 'android' ? 10 : null,
  },
  searchIcon: {
    marginRight: 10,
    marginTop: 8,
    marginLeft: 10,
    color: 'white',
  },
  searchInput: {
    flex: 1,
    backgroundColor: 'transparent',
    height: 40,
    marginRight: 5,
    marginTop: 1,
    color: 'white',
    fontSize: 16,
  },
  sloganBlock: {
    flex: 1,
    justifyContent: 'center',
  },
  sloganText: {
    fontFamily: Platform.OS == 'ios' ? 'Dancing in the Rainbow' : 'Dancing',
    fontSize: 25,
    color: 'white',
    marginLeft: 15,
    marginTop: 2,
    marginBottom: 5,
  },
  settingsBtn: {
    position: 'absolute',
    right: 7,
    top: 2,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
    marginTop: 5,
    marginLeft: 10,
  },
  settingIcon: {
    color: 'white',
  },
});
