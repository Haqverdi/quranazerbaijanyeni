import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Picker,
  Switch,
  TouchableOpacity,
  Platform,
  ActionSheetIOS,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

/** platfor check */
const platform = Platform.OS;

export default class Settings extends Component {
  constructor(props) {
    super(props);
    const { arabCheck, azCheck, fontSize } = this.props.navigation.state.params;
    this.state = {
      fontSize,
      arabCheck,
      azCheck,
    };
  }

  /** Navigation options */
  static navigationOptions = {
    header: null,
  };

  /** Handle changes  */
  _handleChanges = (name, value) => {
    let status = true;
    if (name == 'azCheck' && !value && !this.state.arabCheck) {
      status = false;
    } else if (name == 'arabCheck' && !value && !this.state.azCheck) {
      status = false;
    }

    if (status) {
      this.setState(
        {
          [name]: value,
        },
        async () =>
          await this.props.navigation.state.params._updateSettings(name, value)
      );
    }
  };

  /** ios action buttons handle */
  actionButtonHandle = () => {
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: ['18', '20', '22', '24', '26', '28', '30', 'Cancel'],
        title: 'Mətn ölçüsünü seç:',
        cancelButtonIndex: 7,
        destructiveButtonIndex: 2,
      },
      buttonIndex => {
        // cancel button
        if (buttonIndex == 7) {
          return;
        }
        // check fontSize
        let fontSizeValue = 18 + buttonIndex * 2;
        // save
        this._handleChanges('fontSize', fontSizeValue);
        return;
      }
    );
  };

  render() {
    const { arabCheck, azCheck, fontSize } = this.state;
    const resizeMode = 'center';
    return (
      <View style={style.mainView}>
        <ImageBackground
          style={{
            flex: 1,
            resizeMode,
            position: 'absolute',
            width: '100%',
            height: '100%',
            justifyContent: 'center',
          }}
          source={require('../assets/images/bg.jpg')}
        >
          {/* Back button */}
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={style.backBtn}
          >
            <Icon name="chevron-left" size={40} style={style.backBtnIcon} />
          </TouchableOpacity>
          {/* Arab lang switch */}
          <View style={style.showLangFirst}>
            <Text>Ərəb dilini göstər:</Text>
            <Switch
              onValueChange={() => this._handleChanges('arabCheck', !arabCheck)}
              value={arabCheck}
            />
          </View>
          {/* Az lang switch */}
          <View style={style.showLangSecond}>
            <Text>Azərbaycan dilini göstər:</Text>
            <Switch
              onValueChange={() => this._handleChanges('azCheck', !azCheck)}
              value={azCheck}
            />
          </View>
          {/* if ios show action or show picker */}
          <View style={style.fontBlock}>
            {platform === 'ios' ? (
              <TouchableOpacity
                onPress={this.actionButtonHandle}
                style={{
                  width: '100%',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}
              >
                <Text>Mətn ölçüsünü seç: </Text>
                <Text>{fontSize}</Text>
              </TouchableOpacity>
            ) : (
              <Text>Mətn ölçüsünü seç:</Text>
            )}
            {platform === 'android' && (
              <Picker
                selectedValue={String(fontSize)}
                onValueChange={itemValue => {
                  this._handleChanges('fontSize', Number(itemValue));
                }}
                style={style.fontPicker}
              >
                <Picker.Item label="18" value="18" />
                <Picker.Item label="20" value="20" />
                <Picker.Item label="22" value="22" />
                <Picker.Item label="24" value="24" />
                <Picker.Item label="26" value="26" />
                <Picker.Item label="28" value="28" />
                <Picker.Item label="30" value="30" />
              </Picker>
            )}
          </View>
          {/* about part */}
          <View>
            <Text style={style.authorHead}>
              Ərəbcədən tərcümənin və şərhlərin müəllifi:
            </Text>
            <Text style={style.authorName}>
              Ayətullah Mirzə Əli Meşkini Ərdəbili
            </Text>
            <Text style={style.authorHead}>
              Azərbaycan dilinə tərcümə edənlər:
            </Text>
            <Text style={style.authorName}>Ağabala Mehdiyev</Text>
            <Text style={style.authorName}>Dürdanə Cəfərli</Text>
            <Text style={style.authorHead}>Proqram tərtibi:</Text>
            <Text style={style.authorName}>MirƏli Abdullayev</Text>

            <Text style={style.copyrights}>
              Bütün hüquqlar qorunur © 2013-2019.
            </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

// styles
const style = StyleSheet.create({
  mainView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  backBtn: {
    position: 'absolute',
    top: 40,
    left: 30,
    width: 60,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backBtnIcon: {
    color: '#98C7BF',
  },
  showLangFirst: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 50,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#a0bce4',
    paddingBottom: 5,
    marginTop: 5,
  },
  showLangSecond: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 50,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#a0bce4',
    paddingBottom: 5,
  },
  fontBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 50,
    marginBottom: 40,
    borderBottomWidth: 1,
    borderBottomColor: '#a0bce4',
    paddingBottom: 5,
  },
  fontPicker: {
    height: 30,
    width: 90,
  },
  authorHead: {
    marginTop: 15,
    backgroundColor: 'transparent',
    textAlign: 'center',
    color: '#D3BA7F',
    fontSize: 13,
    fontStyle: 'italic',
  },
  authorName: {
    marginTop: 5,
    backgroundColor: 'transparent',
    textAlign: 'center',
    color: '#80B2A7',
    fontSize: 15,
  },
  copyrights: {
    marginTop: 30,
    backgroundColor: 'transparent',
    textAlign: 'center',
    color: '#6c8f89',
    fontSize: 12,
  },
});
