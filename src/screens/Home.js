import React, { PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  FlatList,
} from 'react-native';
import { HomeHeader } from '../components/HomeHeader';
import SplashScreen from 'react-native-splash-screen';

import { Titles } from '../data/Titles';
import { Bookmarks } from '../data/Bookmarks';
import { Sureler } from '../data/Sureler';
import {
  saveSettingToAsyncStorage,
  saveBookmark,
  readSettingsAndBookmarkFromAsyncStorage,
} from '../lib/helpers';

export default class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: Titles,
      arabCheck: null,
      azCheck: null,
      fontSize: 22,
      bookmarks: Bookmarks,
    };
    this.props.navigation.addListener('willFocus', payload => {
      this.getSettingsFromAsycnStorage();
    });
  }

  static navigationOptions = {
    header: null,
  };

  getSettingsFromAsycnStorage = async () => {
    const {
      arabCheck,
      azCheck,
      fontSize,
      bookmarks,
    } = await readSettingsAndBookmarkFromAsyncStorage();
    this.setState({
      arabCheck: arabCheck,
      azCheck: azCheck,
      fontSize,
      bookmarks,
    });
  };

  _updateSettings = async (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      async () => await saveSettingToAsyncStorage(name, value)
    );
    // await saveSettingToAsyncStorage(name, value);
  };

  /** Search handle part */
  handleSearch = text => {
    this.searchFunc(text);
  };

  searchFunc(searchName) {
    let sureArr = [];
    for (let key in Titles) {
      const object = Titles[key];
      const name = Titles[key].name;

      if (name.toLowerCase().includes(searchName.toLowerCase())) {
        sureArr = [...sureArr, object];
        this.setState({ data: sureArr });
      }
    }
  }
  /** ================ **/

  // goToSettingsScreen
  goToSettingsScreenHandle = () => {
    const { arabCheck, azCheck, fontSize } = this.state;
    this.props.navigation.push('Settings', {
      arabCheck,
      azCheck,
      fontSize,
      _updateSettings: this._updateSettings,
    });
  };

  // save bookmark
  saveBookmarkToStorage = async bookmarks => {
    this.setState(
      {
        bookmarks: bookmarks,
      },
      async () => {
        await saveBookmark(this.state.bookmarks);
      }
    );
  };
  componentDidMount = () => {
    SplashScreen.hide();
  };
  render() {
    const { arabCheck, azCheck, fontSize, bookmarks } = this.state;
    return (
      <SafeAreaView style={style.savMain}>
        <View style={style.firstView}>
          <HomeHeader
            goToSettingsScreen={this.goToSettingsScreenHandle}
            handleSearch={this.handleSearch}
          />
          <FlatList
            style={style.flatList}
            data={this.state.data}
            extraData={this.state}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.push('Read', {
                    itemId: item.id,
                    itemName: item.name,
                    arabCheck: arabCheck,
                    azCheck: azCheck,
                    fontSize: fontSize,
                    data: Sureler[item.id],
                    bookmark: bookmarks,
                    saveBookmarkToStorage: this.saveBookmarkToStorage,
                  })
                }
                style={style.touchOpSure}
              >
                <View style={style.numberBlock}>
                  <View style={style.numberContainer}>
                    <Text style={style.numberText}>{item.id}</Text>
                  </View>
                  <View style={style.nameBlock}>
                    <View style={style.nameBlockCont}>
                      <Text
                        style={
                          azCheck
                            ? style.nameBlockAzerShow
                            : style.nameBlockAzerHide
                        }
                      >
                        {`${item.name}`}
                      </Text>
                      <Text
                        style={
                          arabCheck
                            ? style.nameBlockArabShow
                            : style.nameBlockArabHide
                        }
                      >
                        {`${item.arab}`}
                      </Text>
                    </View>
                    <View style={style.infoBlock}>
                      <Text style={style.infoBlockText}>{`${item.info}`}</Text>
                      <Text>
                        {this.state.bookmarks[item.id].index
                          ? '(' +
                            (this.state.bookmarks[item.id].index + 1) +
                            ')'
                          : ''}
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.name}
          />
        </View>
      </SafeAreaView>
    );
  }
}

// styles
const style = StyleSheet.create({
  savMain: {
    flex: 1,
  },
  firstView: {
    flex: 1,
    backgroundColor: '#E0E0E0',
    paddingBottom: -25,
  },
  flatList: {
    marginBottom: 10,
  },
  touchOpSure: {
    flex: 1,
    backgroundColor: 'white',
    marginHorizontal: 10,
    marginTop: 8,
    borderRadius: 4,
  },
  numberBlock: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: 'center',
  },
  numberContainer: {
    borderRightWidth: 1,
    borderColor: '#DDDDDD',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: 50,
    marginRight: 10,
    paddingTop: 4,
  },
  numberText: {
    fontFamily: 'Husaeni',
    // fontFamily: Platform.OS == 'ios' ? 'Dancing in the Rainbow' : 'Dancing',
    fontSize: 23,
    textAlign: 'center',
    opacity: 0.8,
  },
  nameBlock: {
    flex: 5,
  },
  nameBlockCont: {
    flex: 1,
    flexDirection: 'row',
  },
  nameBlockAzerShow: {
    flex: 2,
    fontFamily: 'PrivaThree',
    fontSize: 18,
    display: 'flex',
  },
  nameBlockAzerHide: {
    flex: 2,
    fontFamily: 'PrivaThree',
    fontSize: 18,
    display: 'none',
  },
  nameBlockArabShow: {
    flex: 2,
    fontFamily: 'Nurkholis',
    fontSize: 18,
    textAlign: 'right',
    paddingRight: 12,
    display: 'flex',
  },
  nameBlockArabHide: {
    flex: 2,
    fontFamily: 'Nurkholis',
    fontSize: 18,
    textAlign: 'right',
    paddingRight: 12,
    display: 'none',
  },
  infoBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginRight: 10,
  },
  infoBlockText: {
    fontSize: 13,
    opacity: 0.7,
    fontStyle: 'italic',
  },
});
