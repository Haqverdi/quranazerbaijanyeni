import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Read extends Component {
  constructor(props) {
    super(props);
    // get data from navigation props
    const {
      itemId,
      itemName,
      arabCheck,
      azCheck,
      fontSize,
      data,
      bookmark,
    } = this.props.navigation.state.params;
    // and set initial State
    this.state = {
      itemId,
      itemName,
      arabCheck,
      azCheck,
      fontSize,
      data,
      bookmark,
    };
  }
  // navigation options, hide header
  static navigationOptions = {
    header: null,
  };

  // handle bookmark change
  bookmarkChangeHandle = (itemId, index) => {
    let updatedBookmark = this.state.bookmark;
    if(updatedBookmark[itemId].index<=0) {
      updatedBookmark[itemId].index = index;
    }
    else {
      updatedBookmark[itemId].index = null;
    }

    // let bookmarks
    this.setState(
      state => ({
        bookmark: updatedBookmark,
      }),
      () => {
        this.props.navigation.state.params.saveBookmarkToStorage(
          this.state.bookmark
        );
      }
    );
  };

  render() {
    const {
      itemId,
      itemName,
      arabCheck,
      azCheck,
      fontSize,
      data,
      bookmark,
    } = this.state;
    return (
      <SafeAreaView style={style.savMain}>
        <View style={style.headerBlock}>
          <TouchableOpacity
            style={style.headerBackBtn}
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon name="chevron-left" size={40} style={style.headerBackIcon} />
          </TouchableOpacity>
          <Text style={style.headeName}>{itemName}</Text>
        </View>
        <View style={style.firstView}>
          <FlatList
            style={style.flatList}
            data={data.az}
            extraData={this.state}
            renderItem={({ item, index }) => (
              <View
                style={
                  index % 2 == 0 ? style.readBlockRose : style.readBlockWhite
                }
              >
                <View style={style.nbBlock}>
                  <View style={style.number}>
                    <Text>{index + 1}</Text>
                  </View>
                  <TouchableOpacity
                    style={style.bookmarkBtn}
                    onPress={() => this.bookmarkChangeHandle(itemId, index)}
                  >
                    <Icon
                      name="bookmark-outline"
                      size={25}
                      style={
                        bookmark && bookmark[itemId].index == index
                          ? style.bookmarkIconRed
                          : style.bookmarkIconBlack
                      }
                    />
                  </TouchableOpacity>
                </View>
                <View style={style.arabBlock}>
                  <Text
                    style={{
                      fontSize: parseInt(fontSize) + 7,
                      opacity: 0.7,
                      textAlign: 'right',
                      display: arabCheck ? 'flex' : 'none',
                      color: '#000',
                    }}
                  >
                    {data.arab[index]}
                  </Text>
                </View>
                <View
                  style={{
                    marginTop: 15,
                    paddingHorizontal: 15,
                    display: azCheck ? 'flex' : 'none',
                  }}
                >
                  <Text
                    style={{
                      fontSize: parseInt(fontSize),
                      opacity: 0.7,
                      fontStyle: 'italic',
                      color: '#000',
                    }}
                  >
                    {`${item}`}
                  </Text>
                </View>
              </View>
            )}
            keyExtractor={(item, index) => index + 'aaa'}
          />
        </View>
      </SafeAreaView>
    );
  }
}

// styles
const style = StyleSheet.create({
  headerBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderBottomWidth: 1,
    backgroundColor: '#FFF',
  },
  headerBackBtn: {
    width: 40,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
  },
  headerBackIcon: {
    color: '#98C7BF',
  },
  headeName: {
    fontFamily: 'PrivaThree',
    fontSize: 23,
  },
  savMain: {
    flex: 1,
    backgroundColor: '#F0F0F0',
  },
  firstView: {
    flex: 1,
    backgroundColor: 'white',
  },
  flatList: {
    paddingBottom: 10,
  },
  readBlockWhite: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor: '#DDD',
  },
  readBlockRose: {
    padding: 15,
    backgroundColor: '#fffbea',
    borderBottomWidth: 1,
    borderBottomColor: '#DDD',
  },
  nbBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  number: {
    fontSize: 14,
    borderWidth: 1,
    borderRadius: 15,
    width: 30,
    height: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bookmarkBtn: {
    position: 'relative',
    top: -10,
    right: -5,
  },
  bookmarkIconBlack: {
    marginRight: 10,
    marginTop: 5,
    marginLeft: 10,
    color: '#666',
  },
  bookmarkIconRed: {
    marginRight: 10,
    marginTop: 5,
    marginLeft: 10,
    color: '#e74c3c',
  },
  arabBlock: {
    marginTop: 10,
  },
});
